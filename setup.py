from setuptools import find_packages, setup

setup(
    name='aeronauts',
    packages=find_packages(),
    version='0.1.0',
    description='A short description of the project.',
    author='AeroNauts',
    license='MIT',
)
