iprofi-hackathon
==============================

# AeroNauts Team

Алексеенко Юлия @j_schreiber

Андриянов Владислав @vvlad1slavv

Дюжев Владимир @Vedddy

Глушанина Мария @mglushanina

# Project description

Our goal was to predict high-count point aerodynamics images using low-count point images. Here is our code to solve this problem.

The given data contained 1000 cases on each body and had an OpenFOAM format. For each experiment we were given 2 versions of an image: high-resolution and low-resolution. Also there were description files which contained two fields: velocity and pressure. 

Our project has two parts: a parsing data one and a neural network for upscaling image resolution. These files' names are: [physics-informed-CNN](notebooks/physics-informed-CNN.ipynb) [super-resolution](notebooks/super_resolution_enhancing_image_quality_using_pretrained_models_ipynb_.ipynb)

Also there is a short presentation summarizing our work, proposals, theories, and results. You can find it by the name [AeroNauts](reports/AeroNauts.pdf)



# Start work with project

## Download data

```
cd data/raw
gdown --no-check-certificate --folder "https://drive.google.com/drive/folders/1oHYiSw6B5u-nK9LRpMCWqft3ucC8MmH7"

python src/data/unzip_dataset.py data/raw/data.zip data/raw
```

## Preprocessing

```
openfoam2206
cd path_to_data
postProcess -func writeCellCentres
```

## ...
