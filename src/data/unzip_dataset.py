import zipfile
import logging
from pathlib import Path

import click

@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_folder', type=click.Path())
def main(input_filepath, output_folder):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    with zipfile.ZipFile(input_filepath, 'r') as zip_ref:
        zip_ref.extractall(output_folder)

if __name__ == '__main__':
    main()
    